﻿using System;
using LinkedListLibrary;

namespace LinkedList
{
  public class Program
  {
    public static void Main(string[] args)
    {
      var myList = new MyLinkedList<int>();

      var newNode1 = new MyLinkedListNode<int>
      {
        Value = 10
      };
      var newNode2 = new MyLinkedListNode<int>
      {
        Value = 20
      };
      var newNode3 = new MyLinkedListNode<int>
      {
        Value = 30
      };
      var newNode4 = new MyLinkedListNode<int>
      {
        Value = 40
      };

      /*myList.AddFirst(newNode1);
      myList.AddFirst(newNode2);
      myList.AddFirst(newNode3);
      myList.AddFirst(newNode4);
      myList.AddFirst(5000);*/

      /*myList.AddLast(newNode1);
      myList.AddLast(newNode2);
      myList.AddLast(newNode3);
      myList.AddLast(newNode4);
      myList.AddLast(5000); */


      /*myList.AddFirst(newNode1);
      myList.AddBefore(newNode1, newNode2);
      myList.AddBefore(newNode2, newNode3);
      myList.AddBefore(newNode3, newNode4);*/

      /*myList.AddFirst(newNode1);
      myList.AddBefore(newNode1, 1000);
      myList.AddBefore(newNode1, 2000);
      myList.AddBefore(newNode2, 3000);
      myList.AddBefore(newNode2, 4000);*/


      /*myList.AddFirst(newNode1);
       myList.AddAfter(newNode1, newNode2);
       myList.AddAfter(newNode2, newNode3);
       myList.AddAfter(newNode3, newNode4);
       myList.AddAfter(newNode4, newNode1);*/

      /*myList.AddFirst(newNode1);
      myList.AddAfter(newNode1, 1000);
      myList.AddAfter(newNode1, 2000);
      myList.AddAfter(newNode1, 3000);*/

      /*myList.AddFirst(newNode1);
      myList.AddLast(newNode2);
      myList.AddLast(newNode3);
      myList.AddLast(newNode4);
      myList.AddLast(5000);
      myList.RemoveFirst();
      myList.RemoveFirst();*/

      /*
      myList.AddFirst(newNode1);
      myList.AddLast(newNode2);
      myList.AddLast(newNode3);
      myList.AddLast(5000);
      myList.RemoveLast();*/

      /*
      myList.AddFirst(newNode1);
      myList.AddLast(newNode2);
      myList.AddLast(newNode3);
      myList.AddLast(newNode4);
      myList.Remove(newNode4);*/

      /*myList.AddFirst(newNode1);
      myList.AddLast(newNode2);
      myList.AddLast(newNode3);
      myList.AddLast(newNode4);
      myList.Remove(30);*/

      myList.AddFirst(newNode1);
      myList.AddLast(newNode2);
      myList.AddLast(newNode3);
      myList.AddLast(newNode4);
      myList.PrintList();
      var value1 = 40;
      Console.WriteLine($"\nDoes {value1} exists? : {myList.Contains(value1)}");
      var value2 = 400;
      Console.WriteLine($"Does {value2} exists? : {myList.Contains(value2)}");


      //myList.PrintList();
    }
  }
}
